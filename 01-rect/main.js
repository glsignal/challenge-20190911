'use strict';

const REPL = require('repl');
const Rect2d = require('./rect2d');

const r1 = new Rect2d([0, 0], [2, 2]);
const r2 = new Rect2d([1, 1], [3, 3]);
const r3 = new Rect2d([0.5, 0.5], [1.5, 1.5]);
const r4 = new Rect2d([4, 4], [5, 5]);

console.log('Rect2d ([x1, y1], [x2, y2])\n');
console.log('r1 =', r1);
console.log('r2 =', r2);
console.log('r3 =', r3);
console.log('r4 =', r4, '\n');

const instance = REPL.start({ prompt: '> ' })

instance.context.Rect2d = Rect2d;
instance.context.r1 = r1;
instance.context.r2 = r2;
