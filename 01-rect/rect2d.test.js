'use strict';

const Rect2d = require('./rect2d');

describe('Rect2d', () => {
  describe('constructor', () => {
    it('throws an exception if fewer than 2 points are specified', () => {
      const fn = () => {
        new Rect2d([0, 0]);
      };

      expect(fn).toThrow();
    });

    it('throws an exception if incorrect types are passed', () => {
      const fn = () => {
        new Rect2d([0, 0], ['foo', 0]);
      };

      expect(fn).toThrow();
    });

    it('assigns coordinates such that (x1,y1) and (x2,y2) are the lower left and upper right respectively', () => {
      const rect = new Rect2d([1, 1], [-1, -1]);

      expect(rect.x1).toBe(-1);
      expect(rect.y1).toBe(-1);
      expect(rect.x2).toBe(1);
      expect(rect.y2).toBe(1);
    });
  });

  describe('#points', () => {
    it('returns an array of points corresponding to the vertices of the rectangle', () => {
      const rect = new Rect2d([1, 2], [3, 4]);

      expect(rect.points).toEqual([
        [1, 2],
        [3, 2],
        [3, 4],
        [1, 4],
      ]);
    });
  });

  describe('#contains', () => {
    it('indicates whether the point is contained within the rectangle', () => {
      const rect = new Rect2d([0, 0], [2, 2]);

      expect(rect.contains([1, 1])).toBe(true);
      expect(rect.contains([0, 0])).toBe(true);
      expect(rect.contains([2, 2])).toBe(true);
      expect(rect.contains([0, 1])).toBe(true);

      expect(rect.contains([2.1, 2.1])).toBe(false);
      expect(rect.contains([-2, -2])).toBe(false);
      expect(rect.contains([1, 3])).toBe(false);
    });

    it('throws an exception if an invalid type is passed', () => {
      const rect = new Rect2d([0, 0], [2, 2]);

      expect(() => { rect.contains() }).toThrow();
      expect(() => { rect.contains([0]) }).toThrow();
    });
  });

  describe('#overlaps', () => {
    it('indicates if the rectangle overlaps a supplied rectangle', () => {
      const r1 = new Rect2d([0, 0], [2, 2]);
      const r2 = new Rect2d([1, 1], [3, 3]);

      expect(r1.overlaps(r2)).toBe(true);
    });

    it('is correct if a rectangle is contained within another rectangle', () => {
      const r1 = new Rect2d([1, 1], [2, 2]);
      const r2 = new Rect2d([0, 0], [3, 3]);

      expect(r1.overlaps(r2)).toBe(true);
    });

    it('is commutative', () => {
      const r1 = new Rect2d([1, 1], [2, 2]);
      const r2 = new Rect2d([0, 0], [3, 3]);

      expect(r1.overlaps(r2)).toEqual(r2.overlaps(r1));
    });

    /**
     * The failing scenario, visually:
     *
     *        +------+
     *        |      |
     *   +----+------+----+
     *   |    |      |    |
     *   +----+------+----+
     *        |      |
     *        +------+
     */
    xit('is correct if all points in the rectangles are mutually outside of each other', () => {
      const r1 = new Rect2d([-1, -4], [1, 4]);
      const r2 = new Rect2d([-4, -1], [4, 1]);

      expect(r1.overlaps(r2)).toBe(true);
    });
  });
});
