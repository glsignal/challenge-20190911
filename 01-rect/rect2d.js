'use strict';

/**
 * A basic rectangle, defined by two opposing points
 */
module.exports = class Rect2d {
  /**
   * Construct a new Rect2d from the supplied points, arranging the coordinates
   * such that the lower left point is represented by (x1,y1) and upper right
   * by (x2,y2)
   *
   * Throws an exception if invalid arguments are passed
   */
  constructor([x1, y1] = [], [x2, y2] = []) {
    // Reject invalid arguments (pretend to be typescript)
    let coords = [x1, y1, x2, y2];

    if (arguments.length < 2 || coords.length < 4) {
      throw new TypeError('All points must be defined');
    }

    coords.map(coord => {
      if (typeof coord !== 'number') {
        throw new TypeError('All inputs must be numeric');
      }
    })

    // We can't use destructured assignment for class members until `this` has
    // been accessed, apparently
    this;

    // Ensure p1 is the lower left corner, and p2 is the upper right corner by
    // ordering the input coordinates (allowing us to simplify the
    // implementation of `contains`)
    [this.y1, this.y2] = [y1, y2].sort();
    [this.x1, this.x2] = [x1, x2].sort();
  }

  /**
   * Compute if a given point is contained within the rectangle by comparing
   * the x and y coordinates against the x and y ranges of this rectangle

   * This is a simplification allowed by our assumption of rectangles always
   * being aligned with the axes
   */
  contains([x, y] = []) {
    if (typeof x !== 'number' || typeof y !== 'number') throw new TypeError('Invalid point');

    if (x > this.x2 || x < this.x1) return false;
    if (y > this.y2 || y < this.y1) return false;

    return true;
  }

  /**
   * Determine if the other shape is overlapping by testing each of its points
   * and whether they are contained within this rectangle
   */
  overlaps(other) {
    return [
      ...other.points.map(this.contains.bind(this)),
      ...this.points.map(other.contains.bind(other)),
    ].includes(true);
  }

  /**
   * Return points for each of the shape's vertices
   */
  get points() {
    return [
      [this.x1, this.y1],
      [this.x2, this.y1],
      [this.x2, this.y2],
      [this.x1, this.y2],
    ];
  }
}
