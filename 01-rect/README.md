# Rectangles

## Assumptions & limitations

Detecting whether two arbitrary rectangles overlap involves a fair amount of
math to cover all cases (I've left a pending test to illustrate one such case),
and I would guess isn't the real point of this exercise.

To that end, I've gone with a set of assumptions to simplify the problem

1. Shapes will always be aligned with the x/y axes (i.e. they will not be
   rotated)
2. If two shapes share a vertex, they are overlapping

I'll acknowledge, but not account for, the pending/failing case in the tests

As a result of (`1.`) shapes may be defined by their lower left corner and
upper right corner (similar to how a bounding box is defined in GeoJSON):

```

   +++++++++++++++# P2
   +              +
P1 #+++++++++++++++

```

---

To extend this to cover all cases (including rectangles that are rotated etc.),
I'd probably do the following, where R1 and R2 are being tested:

1. Check to see if any of the edges from R1 cross any edges from R2 using the
   algorithm in https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection#Given_two_points_on_each_line
   If any of the line segments intersect, the rectangles intersect
2. Translate the coordinate space such that the edges of R1 run parallel to the
   axes, and test whether R1 contains any of R2's points (the translation makes
   this check simpler).
3. Repeat `2.` swapping R2 and R1

Or use a library which implements an efficient algorithm for this.

## Setup

Assumes `node >= 12.4.0`, and `yarn` available

## Run

Use `yarn start` to begin a repl session with the code loaded and some sample
data

## Tests

First install any dependencies with `yarn install`

Run the tests with `yarn test`
