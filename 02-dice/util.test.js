const Util = require('./util');

describe('Util', () => {
  describe('.times', () => {
    it('executes the given function a number of times', () => {
      const fn = jest.fn();

      Util.times(5, fn);

      expect(fn.mock.calls.length).toBe(5);
    });

    it('passes the index of the iteration', () => {
      const fn = jest.fn();

      Util.times(5, fn);

      expect(fn.mock.calls.map(c => c[0])).toEqual([0, 1, 2, 3, 4]);
    });

    it('returns the values of the function calls', () => {
      const fn = (i) => `foo ${i}`;

      const result = Util.times(2, fn);

      expect(result).toEqual(['foo 0', 'foo 1']);
    });
  });
});
