# Dice

Run a simulation N times to evaluate the profitability of a dice game

## Setup

Assumes `node >= 12.4.0`, and `yarn` available

## Run

Use `yarn start` to run the simulation and print the result to the console

## Tests

First install any dependencies with `yarn install`

Run the tests with `yarn test`
