'use strict';

const times = require('./util').times;
const N_SIDES_DIE = 6;
const N_DICE = 2;

class Game {
  constructor({ stake = 0.5, randomNumberFn = Math.random } = {}) {
    this.stake = stake;
    this.numDice = N_DICE;
    this.randomNumberFn = randomNumberFn;
  }

  rollDie() {
    return Math.floor(1 + (this.randomNumberFn() * (N_SIDES_DIE - 1)));
  }

  play() {
    const score = times(this.numDice, () => this.rollDie())
      .reduce((sum, roll) => sum + roll, 0)

    return Game.calculateWinnings(this.stake, score);
  }
}

Game.calculateWinnings = function(stake, score) {
  if (score === 12) return stake * 4;
  if (score === 11) return stake * 3;
  if (score === 10) return stake * 2;
  if (score >= 7) return stake;

  return 0;
}

module.exports = Game;
