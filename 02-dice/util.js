'use strict';

module.exports = {
  times(count, fn) {
    const res = [];

    for (let i = 0; i < count; i++) {
      res.push(fn(i));
    }

    return res;
  }
};
