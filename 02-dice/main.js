'use strict';

const Game = require('./game');
const times = require('./util').times;

const numPlays = 1000;
const stake = 0.5;

const simulator = new Game({ stake });
const result = times(numPlays, () => simulator.play()).reduce((sum, result) => sum + result, 0);

console.log(`Total winnings of ${result} Euro from ${numPlays} plays, with ${stake} Euro stake`);
