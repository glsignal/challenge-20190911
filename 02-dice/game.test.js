'use strict';

const Game = require('./game');

describe('Game', () => {
  describe('#play', () => {
    it('simulates a single round and returns the result', () => {
      Math.random = jest.fn(() => 1)

      expect(new Game().play()).toEqual(2.0);
    });
  });

  describe('#rollDie', () => {
    it('returns a minimum of 1', () => {
      const randomNumberFn = jest.fn(() => 0)

      expect(new Game({ randomNumberFn }).rollDie()).toEqual(1);
    });

    it('returns a maximum of 6', () => {
      const randomNumberFn = jest.fn(() => 1)

      expect(new Game({ randomNumberFn }).rollDie()).toEqual(6);
    });

    it('returns whole numbers', () => {
      const randomNumberFn = jest.fn(() => 0.448908278)

      expect(new Game({ randomNumberFn }).rollDie()).toEqual(3);
    });
  });

  describe('.calculateWinnings', () => {
    const stake = 0.5;

    it('returns the stake * 4 for a roll of 12', () => {
      expect(Game.calculateWinnings(stake, 12)).toEqual(2.0);
    });

    it('returns the stake * 3 for a roll of 11', () => {
      expect(Game.calculateWinnings(stake, 11)).toEqual(1.5);
    });

    it('returns the stake * 2 for a roll of 10', () => {
      expect(Game.calculateWinnings(stake, 10)).toEqual(1.0);
    });

    it('returns the original stake for a roll of 7, 8, or 9', () => {
      expect(Game.calculateWinnings(stake, 7)).toEqual(0.5);
      expect(Game.calculateWinnings(stake, 8)).toEqual(0.5);
      expect(Game.calculateWinnings(stake, 9)).toEqual(0.5);
    });

    it('returns nothing for rolls of 6 or less', () => {
      expect(Game.calculateWinnings(stake, 1)).toEqual(0);
      expect(Game.calculateWinnings(stake, 2)).toEqual(0);
      expect(Game.calculateWinnings(stake, 3)).toEqual(0);
      expect(Game.calculateWinnings(stake, 4)).toEqual(0);
      expect(Game.calculateWinnings(stake, 5)).toEqual(0);
      expect(Game.calculateWinnings(stake, 6)).toEqual(0);
    });
  });
});
