# Currency Converter

A client-side currency converting tool.

Note: there are some warnings from the jest runner about state changes/renders
happening outside of an `act` block, this one's new to me and I haven't figured
out why yet.

## Setup

Assumes `node >= 12.4.0`, and `yarn` available

Install the dependencies with `yarn install`

## Development

Start the dev server with `yarn serve`, and open your browser at
http://localhost:8080/

Webpack should automatically build when files change

## Tests

Run the tests with `yarn test`

## Building for production

Run `yarn build`, the assets from the `dist` folder can be uploaded to the
host's content base
