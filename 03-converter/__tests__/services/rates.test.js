'use strict';

import axios from 'axios';
jest.mock('axios');

import { get } from 'Services/rates';
import { ApiEndpoint } from 'Root/constants';


const fakeResponse = {
  rates: {
    NZD: 1.7426,
    JPY: 120.72,
    USD: 1.1034
  },
  base: 'EUR',
  date: '2019-11-08'
};

describe('Rates', () => {
  describe('.get', () => {
    test('API integration', async () => {
      const base = 'EUR';
      const currencies = ['NZD', 'JPY', 'USD'];
      axios.get.mockResolvedValue({ data: fakeResponse });

      const response = await get(base, currencies);

      expect(axios.get.mock.calls[0][0]).toEqual('https://api.exchangeratesapi.io/latest');
      expect(axios.get.mock.calls[0][1]).toEqual({ params: { base: 'EUR', symbols: 'JPY,NZD,USD' } });

      expect(response).toEqual(fakeResponse);
    });
  });
});
