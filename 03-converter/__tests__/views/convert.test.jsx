'use strict';

import Convert from 'Views/convert';
import { get as getRates } from 'Services/rates';

jest.mock('Services/rates');

const response = {
  base: 'EUR',
  rates: {
    JPY: 120,
    USD: 1.3,
  },
};

let container;
let clock;

beforeEach(() => {
  clock = sinon.useFakeTimers();
});

afterEach(() => {
  clock.restore();
});

describe('Convert', () => {
  it('shows the conversion table for valid inputs', async () => {
    getRates.mockResolvedValue(response);

    const wrapper = mount(<Convert />);

    const currencySelect = wrapper.find('select[name="currency"]');
    const amountInput = wrapper.find('input[name="amount"]');

    amountInput.simulate('change', { target: { value: 1 } });
    currencySelect.simulate('change', { target: { value: 'EUR' } });

    clock.tick();

    expect(wrapper.find('h4').text()).toMatch(/Converting 1 EUR into:/)
  });

  it('hides the table and shows a warning when the input is invalid', async () => {
    getRates.mockResolvedValue(response);

    const wrapper = mount(<Convert />);

    const currencySelect = wrapper.find('select[name="currency"]');
    const amountInput = wrapper.find('input[name="amount"]');

    amountInput.simulate('change', { target: { value: 'blah' } });

    clock.tick();

    expect(wrapper.find('table').length).toBe(0);
    expect(wrapper.text()).toMatch(/Please input a valid number/);
  });
});
