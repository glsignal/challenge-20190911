'use strict';

import ConversionTable from 'Components/conversion-table';

const amount = 2;
const base = 'EUR';
const rates = {
  JPY: 120,
  USD: 1.3,
};

describe('Convert', () => {
  it('renders the conversion rates correctly', () => {
    const wrapper = mount(<ConversionTable amount={amount} base={base} rates={rates} />);

    expect(wrapper.find('h4').text()).toMatch(/Converting 2 EUR into:/)
    expect(wrapper.find('tbody tr:first-child').text()).toMatch('Japanese Yen(JPY)¥ 240.00')
    expect(wrapper.find('tbody tr:last-child').text()).toMatch('US Dollars(USD)$ 2.60')
  });
});

