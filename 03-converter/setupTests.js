import 'regenerator-runtime/runtime';

import React from 'react';
import { act } from 'react-dom/test-utils';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow, mount, render } from 'enzyme';
import * as sinon from 'sinon';
Enzyme.configure({ adapter: new Adapter() });

global.React = React;
global.sinon = sinon;
global.Enzyme = Enzyme;
global.shallow = shallow;
global.mount = mount;
global.render = render;
global.act = act;
