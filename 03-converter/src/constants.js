'use strict';

export const ApiEndpoint = 'https://api.exchangeratesapi.io/latest';

export const CurrencyLabel = {
  EUR: 'Euro',
  JPY: 'Japanese Yen',
  NZD: 'New Zealand Dollars',
  USD: 'US Dollars',
};

export const CurrencySymbol = {
  EUR: '€',
  JPY: '¥',
  NZD: '$',
  USD: '$',
};

export const ExchangePrecision = 2;
