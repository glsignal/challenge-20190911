'use strict';

import { useState, useEffect } from 'react';
import { get as getRates } from 'Services/rates';

export default function(base, requestedRates) {
  const [rates, setRates] = useState({});

  // Refresh the rates when this hook is first used so we can populate the rate
  // data for the UI, and refresh when the base or requested rates change
  useEffect(() => {
    getRates(base, requestedRates).then(({ base, rates }) => setRates(rates));
  }, [base, ...requestedRates]);

  return [rates];
};
