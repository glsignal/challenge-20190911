'use strict';

import { useState, useEffect } from 'react';

// This is largely unfinished at present - the intention is to use the
// conversion API to build a list of available currencies, and allow the user
// to configure which they'd like to switch between (and store that
// configuration in localstorage so it persists)
export default function(requestedRates) {
  const [configuration, setConfigurationState] = useState({
    enabledCurrencies: ['JPY', 'EUR', 'NZD', 'USD'],
  });

  const setConfiguration = function(key, value) {
    setConfigurationState(configuration.merge({ [key]: value }));

    // persist in local storage
  }

  // const onStorageChange = function(evt) { }

  // useEffect(() => {
  //   window.addEventListener('storage', onStorageChange);

  //   return () => {
  //     window.removeEventListener('storage', onStorageChange);
  //   };
  // });

  return [configuration, setConfiguration];
};
