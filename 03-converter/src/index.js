'use strict';

import { createElement } from 'react';
import { render } from 'react-dom';

import 'Root/style.scss';

import App from 'Root/app';

render(
  createElement(App),
  document.getElementById('root')
);
