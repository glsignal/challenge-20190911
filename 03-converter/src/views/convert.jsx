'use strict';

import React, { useState } from 'react';

import { CurrencyLabel } from 'Root/constants';

import useConversionRates from 'Hooks/use-conversion-rates';
import useConfiguration from 'Hooks/use-configuration';

import ConversionTable from 'Components/conversion-table';
import Configuration from 'Components/configuration';

export default function Convert() {
  const [configuration] = useConfiguration();
  const [currency, setCurrency] = useState('EUR');
  const [rates] = useConversionRates(currency, configuration.enabledCurrencies);
  const [amount, setAmount] = useState(1);

  const populated = !!amount;
  const valid = Number.isFinite(amount);

  function onAmountChanged(evt) {
    const parsedValue = parseFloat(evt.target.value);

    setAmount(Number.isNaN(parsedValue) ? evt.target.value : parsedValue);
  }

  return (
    <>
      <div className="hero flex flex--column flex--center">
        <div className="flex-row">
          <h1>Cabriolet</h1>
        </div>

        <div className="flex-row">
          <form className="inline" onSubmit={evt => evt.preventDefault()}>
            <div className="form-group">
              <label htmlFor="amount">Amount</label>

              <div className="form-control">
                <input id="amount" name="amount" type="number" value={amount} onChange={onAmountChanged} />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="currency">Currency</label>

              <div className="form-control">
                <select id="currency" name="currency" value={currency} onChange={evt => setCurrency(evt.target.value)}>
                  <option key={currency} value={currency}>{CurrencyLabel[currency]}</option>)
                  {Object.keys(rates).map(rate => (
                    <option key={rate} value={rate}>{CurrencyLabel[rate]}</option>)
                  )}
                </select>
              </div>
            </div>
          </form>
        </div>
      </div>


      {!valid && (
        <div className="frosted flex flex--column flex--center text-smaller">
          Please input a valid number
        </div>
      )}

      {populated && valid && (
        <ConversionTable amount={amount} base={currency} rates={rates} />
      )}

      {/* TODO: <Configuration /> */}
    </>
  );
}
