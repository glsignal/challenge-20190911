'use strict';

import React from 'react';

import {
  CurrencyLabel,
  CurrencySymbol,
  ExchangePrecision,
} from 'Root/constants';

export default function ConversionTable({ amount, base, rates } = {}) {
  if (!amount || amount === 0) return <></>;
  if (!rates || Object.keys(rates) === []) return (
    <section>
      <p>Waiting for data...</p>
    </section>
  );

  const currencies = Object.keys(rates).filter(currency => currency !== base).sort();

  return (
    <section className="frosted flex flex--column flex--center text-smaller">
      <h4>Converting {amount} {base} into:</h4>

        <div className="flex-row desktop-half-width mobile-full-width">
        <table>
          <thead>
            <tr>
              <th>Currency</th>
              <th></th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            {currencies.map(currency => (
              <tr key={currency}>
                <td>{CurrencyLabel[currency]}</td>
                <td>({currency})</td>
                <td>{CurrencySymbol[currency]} {(amount * rates[currency]).toFixed(ExchangePrecision)}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </section>
  );
}
