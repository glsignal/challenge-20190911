'use strict';

import axios from 'axios';
import { ApiEndpoint } from 'Root/constants';

const fakeRates = {
  base: 'EUR',
  date: '2019-11-08',
  rates: {
    EUR: 1,
    JPY: 120,
    NZD: 1.6,
    USD: 1.15,
  }
};

export function get(base, currencies) {
  return new Promise((resolve, reject) => {
    const symbols = currencies.filter(c => c !== base).sort().join(',');

    const params = { base, symbols };

    axios.get(ApiEndpoint, { params })
      .then(({ data }) => resolve(data))
      .catch(reject);
  });
}
